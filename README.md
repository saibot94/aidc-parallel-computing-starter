
## Demo OpenCV project for Infragrid

OpenCV is installed on the Infragrid cluster for your compiling needs. You can see it installed by running the `module avail` command.

In order to run the code, you first need to load the gcc and computer_vision modules (in this exact order):

```
module load gcc/4.8.1  

module load collection/computer_vision/2014.6 collection/core/2014.6

```


### Compiling

I've added a really simple OpenCV cpp file that you can run using the CMake files listed in here. I think you might need to change the executable files in the CMakeFiles.txt file as well. Now you are all set to compile the code. In order to do this, we use cmake. Create a folder called build in the project folder of your implementation, then run the following commands:

```
cmake ..
make
```

Your executable will now be available to be run on the cluster.

If you need MPI support, you can simply add the
```
find_packages( MPI REQUIRED)
```

line in CMakeLists.txt and recompile (check the files provided as support for each of the needed tasks). You need only include your .cpp files and run cmake with the corresponding starter kits.

Run the program that you've written as usual. MPI programs need to be run with the `mpirun` command, as usual!


### Extra help / step-by-step solution

The full printout of compiling thee test code with the OpenMP package is the following:

```
-sh-4.1$ ls
CMakeLists.txt  hello-cv.cpp
-sh-4.1$ mkdir build
-sh-4.1$ cd build/
-sh-4.1$ cmake ..
-- The C compiler identification is GNU 4.9.0
-- The CXX compiler identification is GNU 4.9.0
-- Check for working C compiler: /u/software/v2/core/bin/cc
-- Check for working C compiler: /u/software/v2/core/bin/cc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Check for working CXX compiler: /u/software/v2/core/bin/c++
-- Check for working CXX compiler: /u/software/v2/core/bin/c++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Try OpenMP C flag = [-fopenmp]
-- Performing Test OpenMP_FLAG_DETECTED
-- Performing Test OpenMP_FLAG_DETECTED - Success
-- Try OpenMP CXX flag = [-fopenmp]
-- Performing Test OpenMP_FLAG_DETECTED
-- Performing Test OpenMP_FLAG_DETECTED - Success
-- Found OpenMP: -fopenmp
-- Configuring done
-- Generating done
-- Build files have been written to: /u/users/guests/cp2014.26/cristian.schuszter/opencv-test/build
-sh-4.1$ make
Scanning dependencies of target DisplayImage
[100%] Building CXX object CMakeFiles/DisplayImage.dir/hello-cv.cpp.o
Linking CXX executable DisplayImage
[100%] Built target DisplayImage
```
