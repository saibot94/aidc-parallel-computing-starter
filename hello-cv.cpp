#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace cv;
using namespace std;

Mat src, src_gray;
Mat dst, detected_edges;

int edgethresh = 1;
int lowThreshold = 50;
int const max_lowThreshold = 100;
int ratio = 3;
int kernel_size = 5;


/**
 * @function CannyThreshold
 * @brief Trackbar callback - Canny thresholds input with a ratio 1:3
 */
void CannyThreshold(int, void*)
{
  /// Reduce noise with a kernel 3x3
  blur( src_gray, detected_edges, Size(3,3) );

  /// Canny detector
  Canny( detected_edges, detected_edges, lowThreshold, lowThreshold*ratio, kernel_size );

  /// Using Canny's output as a mask, we display our result
  dst = Scalar::all(0);

  src.copyTo( dst, detected_edges);
  imwrite("result.jpg", detected_edges);
 }


 int main(int argc, char** argv)
 { 
     if(argc < 3) { 
         cout << "You need to specify <threshold> <image_path>" << endl << endl;
         return -1;
     }
     src = imread(argv[2]);
     lowThreshold = atoi(argv[1]);
     if(!src.data) {
         return -1;
     }

     dst.create(src.size(), src.type());
     cvtColor(src, src_gray, CV_BGR2GRAY);

    /// Show the image
    CannyThreshold(0, 0);

    return 0;
 }


